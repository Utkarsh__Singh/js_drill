// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const prob5 = require('../problems/problem5')
const years = require('../problems/problem4')
let checkdata = (years.prob4())
console.log(checkdata)
test('cars built prior year 2000 ', () => {
  expect(prob5.prob5(checkdata)).toEqual([
  1983, 1990, 1995, 1987, 1996,
  1997, 1999, 1987, 1995, 1994,
  1985, 1997, 1992, 1993, 1964,
  1999, 1991, 1997, 1992, 1998,
  1965, 1996, 1995, 1996, 1999
]
)
})

/* 
Output 5 => [
  1983, 1990, 1995, 1987, 1996,
  1997, 1999, 1987, 1995, 1994,
  1985, 1997, 1992, 1993, 1964,
  1999, 1991, 1997, 1992, 1998,
  1965, 1996, 1995, 1996, 1999
]

25
*/