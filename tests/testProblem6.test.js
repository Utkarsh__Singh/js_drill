// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const prob6=require('../problems/problem6')

let brand1 = 'Audi'
let brand2='BMW'
let expected = ((prob6.prob6(brand1, brand2)))

test(`should print all the cars of ${brand1} and ${brand2}`, () => {
  expect(expected).toEqual("[{\"id\":6,\"car_make\":\"Audi\",\"car_model\":\"riolet\",\"car_year\":1995},{\"id\":8,\"car_make\":\"Audi\",\"car_model\":\"4000CS Quattro\",\"car_year\":1987},{\"id\":25,\"car_make\":\"BMW\",\"car_model\":\"525\",\"car_year\":2005},{\"id\":30,\"car_make\":\"BMW\",\"car_model\":\"6 Series\",\"car_year\":2010},{\"id\":44,\"car_make\":\"Audi\",\"car_model\":\"Q7\",\"car_year\":2012},{\"id\":45,\"car_make\":\"Audi\",\"car_model\":\"TT\",\"car_year\":2008}]")
})


/* 
* Output 6 => [
  {"id":6,"car_make":"Audi","car_model":"riolet","car_year":1995},{"id":8,"car_make":"Audi","car_model":"4000CS Quattro","car_year":1987},{"id":25,"car_make":"BMW","car_model":"525","car_year":2005},{"id":30,"car_make":"BMW","car_model":"6 Series","car_year":2010},{"id":44,"car_make":"Audi","car_model":"Q7","car_year":2012},{"id":45,"car_make":"Audi","car_model":"TT","car_year":2008}
]
*/
