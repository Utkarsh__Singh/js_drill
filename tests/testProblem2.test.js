// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 

const prob2 = require('../problems/problem2')
const data=require('../static/data')
test('Hello', () => {
    expect(prob2.prob2(data.inventory.length - 1)).toBe("Last car is a Lincoln Town Car")
})



// Output2 => "Last car is a Lincoln Town Car"