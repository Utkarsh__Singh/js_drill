// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const prob1 = require('../problems/problem1')
// console.log(prob1)
let car_id = 33
test("hello", () => {
    expect(prob1.prob1(car_id)).toBe("Car 33 is a 2011 Jeep Wrangler")
})

// console.log(prob1(car_id))

// Output1 => "Car 33 is a 2011 Jeep Wrangler"