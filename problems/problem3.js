// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
// Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


const y = require('../static/data')
let x = y.inventory

function prob3(x) {
    for (let i = 0; i < x.length; i++) {
        for (let j = i + 1; j < x.length; j++) {
            if (x[j].car_model.toLowerCase() < x[i].car_model.toLowerCase()) {
                let temp = x[i]
                x[i] = x[j]
                x[j] = temp
            }
        }
    }
    return (JSON.stringify(x))
    // console.log(x)
}

// prob3(x)
module.exports = {
    prob3:prob3,
}