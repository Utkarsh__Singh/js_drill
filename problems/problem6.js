// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const y = require("../static/data")
let x=y.inventory


function prob6(brand1, brand2) {
    let BMWAndAudi=[]
    for (let i = 0; i <x.length; i++) {
        let a=(x[i].car_make.includes(brand1))
        let b = (x[i].car_make.includes(brand2))
        if(a || b) BMWAndAudi.push(x[i])
    }
    // console.log(BMWAndAudi)
    return (JSON.stringify(BMWAndAudi))
}

module.exports = {
    prob6: prob6
}