// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const y = require("../static/data")
let x = y.inventory
function prob5(years) {
    let result=[]
    for (let i = 0; i < years.length; i++) {
        if (years[i] <2000) result.push(years[i])
    }
    // console.log(result)
    return result
}
module.exports = {
    prob5:prob5,
}

// prob5(years())